Description: libsnmp: Move the securityStateRef check into free_securityStateRef()
 Instead of making each free_securityStateRef() caller check the
 securityStateRef pointer, move that check into free_securityStateRef().
Author: Bart Van Assche
Origin: upstream, https://github.com/net-snmp/net-snmp/commit/92ccd5a82a019fbfa835cc8ab2294cf0ca48c8f2
Applied-Upstream: v5.8.1pre2
Reviewed-by: Craig Small <csmall@debian.org>
Last-Update: 2020-06-29 <YYYY-MM-DD, last update of the meta-information, optional>
--- a/snmplib/snmp_api.c
+++ b/snmplib/snmp_api.c
@@ -4011,7 +4011,12 @@
 void
 free_securityStateRef(netsnmp_pdu* pdu)
 {
-    struct snmp_secmod_def *sptr = find_sec_mod(pdu->securityModel);
+    struct snmp_secmod_def *sptr;
+
+    if (!pdu->securityStateRef)
+        return;
+
+    sptr = find_sec_mod(pdu->securityModel);
     if (sptr) {
         if (sptr->pdu_free_state_ref) {
             (*sptr->pdu_free_state_ref) (pdu->securityStateRef);
@@ -4133,9 +4138,7 @@
      * FIX - yes they should but USM needs to follow new EoP to determine
      * which cached values to use 
      */
-    if (pdu->securityStateRef) {
-        free_securityStateRef(pdu);
-    }
+    free_securityStateRef(pdu);
 
     if (error == SNMPERR_USM_NOTINTIMEWINDOW) {
         pdu->securityLevel = SNMP_SEC_LEVEL_AUTHNOPRIV;
@@ -5647,9 +5650,7 @@
     /*
      * Call the security model to free any securityStateRef supplied w/ msg.  
      */
-    if (pdu->securityStateRef != NULL) {
-      free_securityStateRef(pdu);
-    }
+    free_securityStateRef(pdu);
     snmp_free_pdu(pdu);
     return NULL;
   }
@@ -5673,9 +5674,7 @@
     /*
      * Call USM to free any securityStateRef supplied with the message.  
      */
-    if (pdu->securityStateRef) {
-      free_securityStateRef(pdu);
-    }
+    free_securityStateRef(pdu);
 
     for (rp = isp->requests; rp; orp = rp, rp = rp->next_request) {
       snmp_callback   callback;
@@ -5826,7 +5825,7 @@
   /*
    * Call USM to free any securityStateRef supplied with the message.  
    */
-  if (pdu->securityStateRef && pdu->command == SNMP_MSG_TRAP2)
+  if (pdu->command == SNMP_MSG_TRAP2)
     free_securityStateRef(pdu);
 
   if (!handled) {
--- a/agent/snmp_agent.c
+++ b/agent/snmp_agent.c
@@ -1606,10 +1606,10 @@
                 asp, asp->reqinfo));
 
     /* Clean up securityStateRef here to prevent a double free */
-    if (asp->orig_pdu && asp->orig_pdu->securityStateRef)
-        snmp_free_securityStateRef(asp->orig_pdu);
-    if (asp->pdu && asp->pdu->securityStateRef)
-        snmp_free_securityStateRef(asp->pdu);
+    if (asp->orig_pdu)
+ 	snmp_free_securityStateRef(asp->orig_pdu);
+    if (asp->pdu)
+ 	snmp_free_securityStateRef(asp->pdu);
 
     if (asp->orig_pdu)
         snmp_free_pdu(asp->orig_pdu);
